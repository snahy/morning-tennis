/**
 * Vueインスタンス生成
 */
const app = new Vue({
    "data": {
        "items": [
            {
                "court": "",
                "date": "1/4(土)",
                "memo": "",
                "name": ""
            },
            {
                "court": "D",
                "date": "1/5(日)",
                "memo": "",
                "name": "榎本"
            },
            {
                "court": "B",
                "date": "1/11(土)",
                "memo": "",
                "name": "福田ゆき"
            },
            {
                "court": "C",
                "date": "1/12(日)",
                "memo": "",
                "name": "福田ゆき"
            },
            {
                "court": "C",
                "date": "1/13(月)",
                "memo": "",
                "name": "福田ゆき"
            },
            {
                "court": "D",
                "date": "1/18(土)",
                "memo": "",
                "name": "福田あけみ"
            },
            {
                "court": "B",
                "date": "1/19(日)",
                "memo": "",
                "name": "酒井公輔"
            },
            {
                "court": "",
                "date": "1/25(土)",
                "memo": "",
                "name": ""
            },
            {
                "court": "C, D",
                "date": "1/26(日)",
                "memo": "",
                "name": "門岡, 榎本"
            },
            {
                "court": "",
                "date": "2/1(土)",
                "memo": "",
                "name": "抽選前"
            },
            {
                "court": "",
                "date": "2/2(日)",
                "memo": "",
                "name": "抽選前"
            },
            {
                "court": "",
                "date": "2/8(土)",
                "memo": "",
                "name": "抽選前"
            },
            {
                "court": "",
                "date": "2/9(日)",
                "memo": "",
                "name": "抽選前"
            },
            {
                "court": "",
                "date": "2/11(火)",
                "memo": "",
                "name": "抽選前"
            },
            {
                "court": "",
                "date": "2/15(土)",
                "memo": "",
                "name": "抽選前"
            },
            {
                "court": "",
                "date": "2/16(日)",
                "memo": "",
                "name": "抽選前"
            },
            {
                "court": "",
                "date": "2/22(土)",
                "memo": "",
                "name": "抽選前"
            },
            {
                "court": "",
                "date": "2/23(日)",
                "memo": "",
                "name": "抽選前"
            },
            {
                "court": "",
                "date": "2/24(月)",
                "memo": "",
                "name": "抽選前"
            }
        ],
        "update": "2025/01/01 14:18"
    },
    "el": "#app"
})